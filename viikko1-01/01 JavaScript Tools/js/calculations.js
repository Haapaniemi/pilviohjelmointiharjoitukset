

function multiplyBy() {
    var firstnumber = document.getElementById("firstNumber").value;
    var secondnumber = document.getElementById("secondNumber").value;

    var output = null;
    // check the input
    if (isNaN(firstnumber) || isNaN(secondnumber)) {
        document.getElementById("result").innerHTML = "NaN";
    } else {
        document.getElementById("result").innerHTML = firstnumber * secondnumber;
    }

}

function divideBy() {
    var firstnumber = document.getElementById("firstNumber").value;
    var secondnumber = document.getElementById("secondNumber").value;

    // check the input
    if (isNaN(firstnumber) || isNaN(secondnumber)) {
        document.getElementById("result").innerHTML = "NaN";
    }
    if (firstnumber == 0 || secondnumber == 0) {
        document.getElementById("result").innerHTML = "Can't divide by zero."
    } else {
        document.getElementById("result").innerHTML = firstnumber / secondnumber;
    }

}

