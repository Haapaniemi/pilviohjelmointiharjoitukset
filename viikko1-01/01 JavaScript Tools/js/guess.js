"use strict";

let targetNumber = Math.floor(Math.random() * 10) + 1;
var tries = 0;

function init() {
	document.getElementById("theform").addEventListener("submit", function (event) {
		event.preventDefault();
		check(document.getElementById("number").value);
	});

}

function check(value) {
	if (value == targetNumber) {
		showWin();
		tries = 0;
	} else if (tries > 5) {
		showLoss();
	} else if (value != targetNumber) {
		showError();
		tries++;
	}
}

function showWin() {
	var parent = document.getElementById("div1");
	var child = document.getElementById("theform");
	parent.removeChild(child);
	console.log("You guessed it right!");
	window.alert("You win!");
}

function showError() {
	console.log("Your guess was incorrect, guess again.");
	window.alert("Your guess was incorrect, try again.");
}

function showLoss() {
	var parent = document.getElementById("div1");
	var child = document.getElementById("theform");
	parent.removeChild(child);
	console.log("You guessed 5 times, you lose.");
	window.alert("You lose.");
}

init();