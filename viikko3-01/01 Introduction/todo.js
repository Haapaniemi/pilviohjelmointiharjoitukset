'use strict'

/* the readline-sync module allows NodeJS to read input from the terminal. */
var readline = require('readline-sync')

/* Here we declare an empty array which will be used to store the todo list items. */
var items = []

/* the do-while loop has its exit condition at the end which means it always executes at least once. */
do {
	/* We capture the text input by the user. This is then converted into a String and finally any _whitespace_ (newline characters, spaces, etc) are removed. */
	// try switching for a let... or a const...
	var input = String(readline.question('enter command: ')) 
	/* The indexOf() function looks for the first ocurrance of the supplied string
  parameter or -1 if it is not found. Notice the use of === to compare the two values,
  this is a 'strict' comparison and should always be used instead of ==. It checks for
  equal values and equal type. */

	if (input.indexOf('add ') === 0) {
		/* This time the indexOf() function is used to locate the first space character. */
		var space = input.indexOf(' ')
		/* the substring() function returns the string after the specified position. This will include the space character and so the result is trimmed of any whitespace. */
		var item = input.substring(space+1).trim().toLowerCase()
		/* console.log() prints to the terminal. */
		var checkDup = items.indexOf(item)
		if(checkDup == -1){
			console.log('adding "'+item+'"')
			/* All arrays have a built-in push() function which appends an item to their end. */
		  items.unshift(item)
		}else{
			console.log('Duplicates are not allowed!')
		}
		
		
	}

	if (input.indexOf('remove ') === 0) {
		/* This time the indexOf() function is used to locate the first space character. */
		var space = input.indexOf(' ')
		/* the substring() function returns the string after the specified position. This will include the space character and so the result is trimmed of any whitespace. */
		var item = input.substring(space+1).trim().toLowerCase()
		/* console.log() prints to the terminal. */
		var checkIteminList = items.indexOf(item)
		if(checkIteminList !== -1){
			console.log('removing "'+item+'"')
			/* All arrays have a built-in push() function which appends an item to their end. */
		  items.splice(checkIteminList, 1)
		}else{
			console.log('No such item in the list!')
		}
		
		
	}
  
  
	if (input.indexOf('list') === 0) {
		/* Here we use a for...next loop to interate through all the array indexes. The let keyword defines a variable with _block_ scope (more on this later). */
		for (var i=0; i< items.length; i++) {
			/* Here we reference the array index. */
			console.log(i+'. '+items[i])
		}
	}
	/* the code will loop unless the input was _exit_. Notice the use of !== in the comparison instead of the more typical !=. This is a strict negating comparison. This means 'not equal and not equal type' */
} while (input !== 'exit')

//### THE ANSWERS FOR THE QUESTIONS ###//
// If we change var input to const input, the program will crash because input will be undefined. // Changing it to let has the same effect.
// Changing the array type from var to const, has really no effect on functionality.
// If we swap from push() to unshift, functionality of the script stays the same apart from the fact that item added first is last in the list.